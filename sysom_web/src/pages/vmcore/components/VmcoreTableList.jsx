import { useRef } from "react";
import ProTable from "@ant-design/pro-table";
import { useIntl, FormattedMessage } from 'umi';

const VmcoreTableList = (props) => {
  const actionRef = useRef();
  const intl = useIntl();

  const columns = [
    {
      title: <FormattedMessage id="pages.vmcore.hostname" defaultMessage="Host name" />,
      dataIndex: "hostname",
      valueType: "textarea",
    },
    {
      title: "IP",
      dataIndex: "ip",
      hideInSearch: true,
      valueType: "textarea",
    },
    {
      title: <FormattedMessage id="pages.vmcore.downtime" defaultMessage="Down time" />,
      dataIndex: "core_time",
      hideInSearch: true,
      valueType: "dateTime",
      sorter: (a, b) => {
        const poa = parseInt(a.core_time.replace(/-/g,"").replace(/:/g,"").replace(/T/g,""),10);
        const pob = parseInt(b.core_time.replace(/-/g,"").replace(/:/g,"").replace(/T/g,""),10);
        return poa - pob;
      },
    },
    {
      title: <FormattedMessage id="pages.vmcore.timerange" defaultMessage="Time range" />,
      dataIndex: "core_time",
      valueType: "dateRange",
      hideInTable: true,
      search: {
        transform: (value) => {
          return {
            startTime: value[0],
            endTime: value[1],
          };
        },
      },
    },
    {
      title: <FormattedMessage id="pages.vmcore.kernelversion" defaultMessage="Kernel version" />,
      dataIndex: "ver",
      valueType: "texarea",
    },
    {
      title: "Vmcore",
      dataIndex: "vmcore_check",
      hideInSearch: true,
      valueEnum: {
        false: {
          text: <FormattedMessage id="pages.vmcore.no" defaultMessage="No" />,
          status: "Default",
        },
        true: {
          text: <FormattedMessage id="pages.vmcore.yes" defaultMessage="Yes" />,
          status: "Success",
        },
      },
    },
    {
      title: <FormattedMessage id="pages.vmcore.solution" defaultMessage="Solution" />,
      dataIndex: "issue_check",
      hideInSearch: true,
      valueEnum: {
        false: {
          text: <FormattedMessage id="pages.vmcore.no" defaultMessage="No" />,
          status: "Default",
        },
        true: {
          text: <FormattedMessage id="pages.vmcore.yes" defaultMessage="Yes" />,
          status: "Success",
        },
      },
    },
    {
      title: <FormattedMessage id="pages.vmcore.outagedetails" defaultMessage="Outage details" />,
      dataIndex: "option",
      valueType: "option",
      render: (_, record) => [
        <a key="showDetail" href={"/vmcore/detail/" + record.id}>
          <FormattedMessage id="pages.vmcore.view" defaultMessage="View" />
        </a>,
      ],
    },
  ];
  return (
    <ProTable
      headerTitle={props.headerTitle}
      actionRef={actionRef}
      rowKey="id"
      columns={columns}
      params={props.params}
      request={props.request}
      pagination={props.pagination}
      search={props.search}
      postData={props.postData}
    />
  );
};

export default VmcoreTableList;
