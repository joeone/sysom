import { Statistic } from 'antd';
import { useState, useEffect } from 'react';
import ProCard from '@ant-design/pro-card';
import RcResizeObserver from 'rc-resize-observer';
import { getStatistics } from '../service';
import { useIntl, FormattedMessage } from 'umi';

const { Divider } = ProCard;

const VmcoreCard = () => {
  const [responsive, setResponsive] = useState(false);
  const [StatisticList, setStatisticList] = useState()
  const intl = useIntl();

  useEffect(async () => {
    const data = await getStatistics()
    setStatisticList(data)
  }, [])

  return (
    <RcResizeObserver
      key="resize-observer"
      onResize={(offset) => {
        setResponsive(offset.width < 596);
      }}
    >
      <ProCard.Group title={intl.formatMessage({
        id: 'pages.vmcore.coreindex',
        defaultMessage: 'Core index',
      })} direction={responsive ? 'column' : 'row'}>
        <ProCard>
          <Statistic title={intl.formatMessage({
            id: 'pages.vmcore.totalnumberlast30days',
            defaultMessage: 'Total number of outages in the last 30 days',
          })} value={StatisticList?.vmcore_30days} valueStyle={{ color: "red" }} />
        </ProCard>
        <Divider type={responsive ? 'horizontal' : 'vertical'} />
        <ProCard>
          <Statistic title={intl.formatMessage({
            id: 'pages.vmcore.totalnumberlast7days',
            defaultMessage: 'Total number of outages in the last 7 days',
          })} value={StatisticList?.vmcore_7days} valueStyle={{ color: "red" }} />
        </ProCard>
        <Divider type={responsive ? 'horizontal' : 'vertical'} />
        <ProCard>
          <Statistic title={intl.formatMessage({
            id: 'pages.vmcore.monthlybreakdownprobability',
            defaultMessage: 'Monthly breakdown probability',
          })} value={StatisticList?.rate_30days} precision={2} suffix="%" valueStyle={{ color: "red" }} />
        </ProCard>
        <Divider type={responsive ? 'horizontal' : 'vertical'} />
        <ProCard>
          <Statistic title={intl.formatMessage({
            id: 'pages.vmcore.outageratelast7days',
            defaultMessage: 'Outage rate in the last 7 days',
          })} value={StatisticList?.rate_7days} precision={2} suffix="%" valueStyle={{ color: "red" }} />
        </ProCard>
      </ProCard.Group>
    </RcResizeObserver>
  );
}
export default VmcoreCard